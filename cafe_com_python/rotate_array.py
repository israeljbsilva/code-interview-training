from itertools import chain
from typing import Iterable, List


def rotate(_list: List, k: int):
    """
    https://www.programcreek.com/2015/03/rotate-array-in-java/
    Gire uma matriz de n elementos para a direita por k passos.
    Por exemplo, com n = 7 ek = 3, a matriz [1,2,3,4,5,6,7] é girada para [5,6,7,1,2,3,4].
    Quantas maneiras diferentes você conhece de resolver esse problema?
    :return:

    >>> new_list = [1, 2, 3, 4, 5, 6, 7]
    >>> list(rotate(new_list, 3))
    [5, 6, 7, 1, 2, 3, 4]
    >>> for element in rotate_bad_solution(new_list, 1):
    ...     print(element)
    ...
    7
    1
    2
    3
    4
    5
    6
    """
    len_list = len(_list)
    first_slice_index = range(len_list-k, len_list)
    second_slice_index = range(len_list-k)
    rotated_indexes = chain(first_slice_index, second_slice_index)
    for rotated_index in rotated_indexes:
        yield _list[rotated_index]


def rotate_bad_solution(iterable: Iterable, k: int):
    """
    Gire uma matriz de n elementos para a direita por k passos.
    Por exemplo, com n = 7 ek = 3, a matriz [1,2,3,4,5,6,7] é girada para [5,6,7,1,2,3,4].
    Quantas maneiras diferentes você conhece de resolver esse problema?
    :return:

    >>> _list = [1, 2, 3, 4, 5, 6, 7]
    >>> rotate_bad_solution(_list, 3)
    [5, 6, 7, 1, 2, 3, 4]
    >>> for element in rotate_bad_solution(_list, 1):
    ...     print(element)
    ...
    7
    1
    2
    3
    4
    5
    6

    >>> next(iter(rotate_bad_solution(_list, 3)))
    5
    """
    new_list = list(iterable)
    first_slice = new_list[-k:]
    second_slice = new_list[:-k]
    return first_slice + second_slice
