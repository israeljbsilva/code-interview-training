def reverse_words(phrase: str) -> str:
    """
    https://www.programcreek.com/2014/05/leetcode-reverse-words-in-a-string-ii-java/
    Dada uma string de entrada, inverta a string palavra por palavra.
    Uma palavra é definida como uma sequência de caracteres sem espaço.
    A string de entrada não contém espaços iniciais ou finais e as palavras são sempre separadas por um único espaço.
    Por exemplo,
    Dado s = "o céu é azul",
    retorne "azul é céu o".
    
    >>> reverse_words('o céu é azul')
    'azul é céu o'

    :param phrase: 
    :return: 
    """
    separate_words = phrase.split(' ')
    separate_words.reverse()
    return ' '.join(separate_words)


def reverse_words_constant_space():
    """
    https://www.programcreek.com/2014/05/leetcode-reverse-words-in-a-string-ii-java/
    Dada uma string de entrada, inverta a string palavra por palavra.
    Uma palavra é definida como uma sequência de caracteres sem espaço.
    A string de entrada não contém espaços iniciais ou finais e as palavras são sempre separadas por um único espaço.
    Por exemplo,
    Dado s = "o céu é azul",
    retorne "azul é céu o".

    TODO: Você poderia fazer isso no local, sem alocar espaço extra?

    >>> reverse_words_constant_space()
    'azul é céu o'
    """
    pass
