def fizz_buzz(n: int):
    """
    Criar função fizz_buzz, com parametro n.
    Receber um numero n e imprimir os numeros de 1 até n
    Quando o numero for divisivel por 2 printar fizz
    Quando o numero for divisivel por 3 printar buzz
    Quando pelos dois, imprimir fizzbuzz
    >>> fizz_buzz(6)
    1
    fizz
    buzz
    fizz
    5
    fizzbuzz

    :param n:
    :return:
    """
    for index in range(1, n + 1):
        if index % (3*2) == 0:
            print('fizzbuzz')
        elif index % 2 == 0:
            print('fizz')
        elif index % 3 == 0:
            print('buzz')
        else:
            print(index)


def fizz_buzz_outra_forma(n: int):
    """
    Criar função fizz_buzz, com parametro n.
    Receber um numero n e imprimir os numeros de 1 até n
    Quando o numero for divisivel por 2 printar fizz
    Quando o numero for divisivel por 3 printar buzz
    Quando pelos dois, imprimir fizzbuzz
    >>> fizz_buzz_outra_forma(6)
    1
    fizz
    buzz
    fizz
    5
    fizzbuzz

    :param n:
    :return:
    """
    for index in range(1, n + 1):
        resultado = ''
        if index % 2 == 0:
            resultado = 'fizz'
        if index % 3 == 0:
            resultado += 'buzz'

        print(resultado if resultado != '' else index)
