def contar_letras(s: str):
    """
    Retornar a frequencia com que as letras ocorrem na string. Contar as letras.
    >>> contar_letras('israel')
    {'i': 1, 's': 1, 'r': 1, 'a': 1, 'e': 1, 'l': 1}
    >>> contar_letras('banana')
    {'b': 1, 'a': 3, 'n': 2}

    :param s:
    :return:
    """

    frequencia_letras = {}
    for letra in s:
        frequencia_letras[letra] = frequencia_letras.get(letra, 0) + 1
    return frequencia_letras


def contar_letras_outra_forma(s: str):
    from collections import Counter

    """
    >>> contar_letras_outra_forma('israel')
    {'i': 1, 's': 1, 'r': 1, 'a': 1, 'e': 1, 'l': 1}
    >>> contar_letras_outra_forma('banana')
    {'b': 1, 'a': 3, 'n': 2}

    :param s:
    :return:
    """

    return dict(Counter(s))
